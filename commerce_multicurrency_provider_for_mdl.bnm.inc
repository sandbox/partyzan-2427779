<?php

/**
 * Fetch the currency exchange rates for the requested currency combination.
 * Use bnm.md as provider.
 *
 * Return an array with the array(target_currency_code => rate) combination.
 *
 * @param string $currency_code
 *   Source currency code.
 * @param array $target_currencies
 *   Array with the target currency codes.
 *
 * @return array
 *   Array with the array(target_currency_code => rate) combination.
 */
function commerce_multicurrency_mdl_exchange_rate_sync_provider_bnm($currency_code, $target_currencies) {
  $data = cache_get(__FUNCTION__, 'cache');
  if (!$data) {
    $bnm_rates = array();
    if (($xml = @simplexml_load_file('http://www.bnm.md/ru/official_exchange_rates?get_xml=1&date='.date('d.m.Y'))) && @count($xml->Valute)) {
      foreach ($xml->Valute as $valute) {
        $rate = (float) str_replace(',', '.', (string) $valute->Value);
        $bnm_rates[(string) $valute->CharCode] = empty($rate) ? 0 : $valute->Nominal / $rate;
      }
      cache_set(__FUNCTION__, $bnm_rates, 'cache', time() + 3600);
    }
    else {
      watchdog(
        'commerce_multicurrency', 'Rate provider bnm: Unable to fetch / process the currency data of @url',
        array('@url' => 'http://www.bnm.md/ru/official_exchange_rates?get_xml=1&date='.date('d.m.Y')),
        WATCHDOG_ERROR
      );
    }
  }
  else {
    $bnm_rates = $data->data;
  }

  $rates = array();
  foreach ($target_currencies as $target_currency_code) {
    if ($currency_code == 'MDL' && isset($bnm_rates[$target_currency_code])) {
      $rates[$target_currency_code] = $bnm_rates[$target_currency_code];
    }
    elseif (isset($bnm_rates[$currency_code]) && $target_currency_code == 'MDL') {
      $rates[$target_currency_code] = 1 / $bnm_rates[$currency_code];
    }
    elseif (isset($bnm_rates[$currency_code]) && isset($bnm_rates[$target_currency_code])) {
      $rates[$target_currency_code] = $bnm_rates[$target_currency_code] / $bnm_rates[$currency_code];
    }
  }

  return $rates;
}
