National Bank of Moldova  (bnm.md) as currency exchange rate sync
provider for Commerce multicurrency module.

DEPENDENCIES
Commerce Multicurrency provider for MDL depends on the Commerce multicurrency 
module.

INSTALLATION
Install the module as usual.

CONFIGURATION
1. Select "National Bank of Moldova" as sync provider on currency
   conversion settings page: admin/commerce/config/currency/conversion
2. Run cron or sync manually to synchronize the rates.



The original module was created in Estonia by company Brilliant Solutions
